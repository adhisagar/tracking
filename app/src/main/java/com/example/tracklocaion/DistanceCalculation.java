package com.example.tracklocaion;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.FragmentActivity;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.libraries.places.api.model.Place;
import com.google.maps.android.SphericalUtil;

import java.text.DecimalFormat;
import java.util.ArrayList;

public class DistanceCalculation extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap gMap;
    private LatLng sourcelatLng,destLatln;
    double sourceLatitude,sourceLongitude,destLatitude,destLongitude;
    private MarkerOptions place1, place2;
    private Button calculateDist;
    double d,sp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_distance_calculation);

        SupportMapFragment supportMapFragment= (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.mapNearBy);

        supportMapFragment.getMapAsync(this);
        sourceLatitude=28.571100;
        sourceLongitude=77.328529;
        destLatitude=28.640520;
        destLongitude=77.282707;
        ActivityCompat.requestPermissions(this,new String[]{Manifest.permission.SEND_SMS}, PackageManager.PERMISSION_GRANTED);
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, PackageManager.PERMISSION_GRANTED);
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.INTERNET}, PackageManager.PERMISSION_GRANTED);


        place1=new MarkerOptions().position(new LatLng(sourceLatitude,sourceLongitude));
        place2=new MarkerOptions().position(new LatLng(destLatitude,destLongitude));

        sourcelatLng=new LatLng(sourceLatitude,sourceLongitude);
        destLatln=new LatLng(destLatitude,destLongitude);

        calculateDist=findViewById(R.id.calc_dist);
        calculateDist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                calculationByDistance(sourcelatLng,destLatln);


                d = calculationByDistance(sourcelatLng , destLatln);
                ArrayList<LatLng> points = new ArrayList<>();

                points.add(sourcelatLng);
                points.add(destLatln);

                d = totalDistance(points);
                Log.i("double",""+d);

                sp= SphericalUtil.computeDistanceBetween(sourcelatLng, destLatln);
                Log.i("sp",""+sp);
            }
        });

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

        gMap=googleMap;
        gMap.addMarker(place1).setTitle("Source");
        gMap.addMarker(place2).setTitle("dESTINATION");

        CameraPosition cameraPosition=CameraPosition.builder()
                .target(new LatLng(sourceLatitude,sourceLongitude))
                .zoom(10)
                .bearing(0)
                .tilt(45)
                .build();
        gMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition),5000,null);
    }



    public double calculationByDistance(LatLng StartP, LatLng EndP) {
        int Radius=6371;//radius of earth in Km
        double lat1 = StartP.latitude;
        double lat2 = EndP.latitude;
        double lon1 = StartP.longitude;
        double lon2 = EndP.longitude;
        double dLat = Math.toRadians(lat2-lat1);
        double dLon = Math.toRadians(lon2-lon1);
        double a = Math.sin(dLat/2) * Math.sin(dLat/2) +
                Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2)) *
                        Math.sin(dLon/2) * Math.sin(dLon/2);
        double c = 2 * Math.asin(Math.sqrt(a));
        double result=Radius * c;
        Log.i("result",""+result);
        return result;
    }


    public double totalDistance(ArrayList<LatLng> points)
    {
        double totalDist = 0;

        for(int i = 0 ; i < points.size() - 1 ; i++)
        {
            totalDist += calculationByDistance(points.get(i) , points.get(i+1));
        }
        return totalDist;
    }




/*
    public double CalculationByDistance(LatLng StartP, LatLng EndP) {
        int Radius=6371;//radius of earth in Km
        double lat1 = StartP.latitude;
        double lat2 = EndP.latitude;
        double lon1 = StartP.longitude;
        double lon2 = EndP.longitude;
        double dLat = Math.toRadians(lat2-lat1);
        double dLon = Math.toRadians(lon2-lon1);
        double a = Math.sin(dLat/2) * Math.sin(dLat/2) +
                Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2)) *
                        Math.sin(dLon/2) * Math.sin(dLon/2);
        double c = 2 * Math.asin(Math.sqrt(a));
        double valueResult= Radius*c;
        double km=valueResult/1;
        DecimalFormat newFormat = new DecimalFormat("##.##");
        int kmInDec =  Integer.valueOf(newFormat.format(km));
        double meter=valueResult%1000;
        int  meterInDec= Integer.valueOf(newFormat.format(meter));
        Log.i("Radius Value",""+valueResult+"   KM  "+kmInDec+" Meter   "+meterInDec);
        Log.i("vALUE","");
        return Radius * c;
    }

 */
}
