package com.example.tracklocaion;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.FragmentActivity;

import android.Manifest;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;

public class DriverCallingActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap gMap;
    private LatLng sourcelatLng,destLatln;
    double sourceLatitude,sourceLongitude,destLatitude,destLongitude;
    private MarkerOptions place1, place2;
    double d;
    private Button searchRideBtn;
    String PREFS_NAME = "auth_info";
    LinearLayout ll_call, ll_share, ll_cancel;
    String driver_name, cab_no, cab_id, otp, fare, driver_phone, ride_id;
    TextView cab_no_a, cab_no_b, ride_otp, ride_driver_name, ride_fare;
    Marker nearby_cab;
    RelativeLayout driver_info;
    Handler handler;
    boolean isSourceSet = false, tripStarted = false;
    ImageView cab;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_driver_calling);


        SupportMapFragment supportMapFragment= (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.rider_map);

        supportMapFragment.getMapAsync(this);


        driver_info = findViewById(R.id.driver_details);

        driver_info.setVisibility(View.GONE);

        cab=findViewById(R.id.cab);
        cab.setVisibility(View.GONE);

        sourceLatitude=28.571100;
        sourceLongitude=77.328529;
        destLatitude=28.640520;
        destLongitude=77.282707;
        ActivityCompat.requestPermissions(this,new String[]{Manifest.permission.SEND_SMS}, PackageManager.PERMISSION_GRANTED);
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, PackageManager.PERMISSION_GRANTED);
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.INTERNET}, PackageManager.PERMISSION_GRANTED);


        place1=new MarkerOptions().position(new LatLng(sourceLatitude,sourceLongitude));
        place2=new MarkerOptions().position(new LatLng(destLatitude,destLongitude));

        sourcelatLng=new LatLng(sourceLatitude,sourceLongitude);
        destLatln=new LatLng(destLatitude,destLongitude);


        ArrayList<LatLng> points = new ArrayList<>();

        points.add(sourcelatLng);
        points.add(destLatln);

        d=totalDistance(points);
        Log.i("distance",""+d);

        searchRideBtn=findViewById(R.id.search_ride);
        searchRideBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Log.i("enter","Entered");
                    String api_url = "https://nearcabs.000webhostapp.com/api/book_cab.php";

                    double src_lat = sourceLatitude;
                    double src_lng = sourceLongitude;

                    double dest_lat = destLatitude;
                    double dest_lng = destLongitude;

                    SharedPreferences sharedPreferences = getSharedPreferences(PREFS_NAME, MODE_PRIVATE);

                    String user_id = sharedPreferences.getString("id", null);

                    String book_now_request = "user_id=" + URLEncoder.encode(user_id, "UTF-8") + "&src_lat=" + URLEncoder.encode(String.valueOf(src_lat), "UTF-8") + "&src_lng=" + URLEncoder.encode(String.valueOf(src_lng), "UTF-8") + "&dest_lat=" + URLEncoder.encode(String.valueOf(dest_lat), "UTF-8") + "&dest_lng=" + URLEncoder.encode(String.valueOf(dest_lng), "UTF-8");

                    JSONObject response_data = call_api(api_url, book_now_request);
                    if (response_data.getString("status").equals("1")) {
                        if (nearby_cab != null) {
                            nearby_cab.remove();
                        }

//                        MarkerOptions markerOptions1=new MarkerOptions();
                        JSONObject book_cab_response_data = response_data.getJSONObject("data");

                        ride_id = book_cab_response_data.getString("ride_id");

                        cab_no = book_cab_response_data.getString("cab_no");
                        cab_id = book_cab_response_data.getString("cab_id");
                        driver_name = book_cab_response_data.getString("driver_name");
                        driver_phone = book_cab_response_data.getString("driver_phone");
                        otp = "OTP : " + book_cab_response_data.getString("otp");
                        fare = "Rs. " + book_cab_response_data.getString("fare");

                        cab_no_a.setText(cab_no.split(" ")[0]);
                        cab_no_b.setText(cab_no.split(" ")[1]);

                        ride_driver_name.setText(driver_name);
                        ride_otp.setText(otp);

                        ride_fare.setText(fare);


                        ll_call.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                Intent callIntent = new Intent(Intent.ACTION_CALL);
                                callIntent.setData(Uri.parse("tel:" + driver_phone));

                                if (ActivityCompat.checkSelfPermission(getApplicationContext(),
                                        Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                                    return;
                                }
                                startActivity(callIntent);
                            }
                        });


                        ll_share.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                Intent sharingIntent = new Intent(Intent.ACTION_SEND);
                                sharingIntent.setType("text/plain");
                                String shareBody = driver_name + " (" + driver_phone + ") is on the way in Cab number " + cab_no + ". You are paying " + fare + " for this ride. Share " + otp + " with the driver to start the ride.";
                                sharingIntent.putExtra(Intent.EXTRA_SUBJECT, "Near Cabs Booking");
                                sharingIntent.putExtra(Intent.EXTRA_TEXT, shareBody);
                                startActivity(Intent.createChooser(sharingIntent, "Share via"));
                            }
                        });


                        ll_cancel.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                try {
                                    String cancel_api_url = "https://nearcabs.000webhostapp.com/api/cancel_book_cab.php";
                                    String cancel_book_now_request = "ride_id=" + URLEncoder.encode(ride_id, "UTF-8") + "&cab_id=" + URLEncoder.encode(cab_id, "UTF-8");

                                    JSONObject cancel_response_data = call_api(cancel_api_url, cancel_book_now_request);
                                    if (cancel_response_data.getString("status").equals("1")) {
                                        Toast.makeText(getApplicationContext(), "Booking Cancelled", Toast.LENGTH_SHORT).show();
                                        driver_info.setVisibility(View.GONE);
                                        searchRideBtn.setVisibility(View.VISIBLE);
                                    }
                                } catch (Exception e) {
//                                 Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                                }
                            }
                        });


                        searchRideBtn.setVisibility(View.GONE);
                        driver_info.setVisibility(View.VISIBLE);



//                        updateNearbyCabPosition();

                        handler.postDelayed(runnable, 0);


                    } else {
                        Toast.makeText(getApplicationContext(), "No cabs nearby", Toast.LENGTH_LONG).show();
                    }


                } catch (Exception e) {
//                    Toast.makeText(getApplicationContext(),e.getMessage(),Toast.LENGTH_LONG).show();
                    Log.i("Exit",""+e.getMessage());
                }

            }
        });

    }


    public Runnable runnable = new Runnable() {
        @Override
        public void run() {
            MarkerOptions markerOptions1;
            try {

                String api_url = "https://nearcabs.000webhostapp.com/api/get_cab_location.php";

                String get_cab_location_request = "cab_id=" + URLEncoder.encode(cab_id, "UTF-8") + "&ride_id=" + URLEncoder.encode(ride_id, "UTF-8");

                JSONObject response_data = call_api(api_url, get_cab_location_request);

//                Toast.makeText(getApplicationContext(), response_data.toString(), Toast.LENGTH_LONG).show();

                if (response_data.getString("status").equals("1")) {
                    if (nearby_cab != null) {
                        nearby_cab.remove();
                    }

                    markerOptions1 = new MarkerOptions();
                    JSONObject get_cab_position_response_data = response_data.getJSONObject("data");

                    LatLng nearby_cab_position = new LatLng(Double.parseDouble(get_cab_position_response_data.getString("cab_lat")), Double.parseDouble(get_cab_position_response_data.getString("cab_lng")));
                    markerOptions1.position(nearby_cab_position);

                    BitmapDrawable bitmapDrawable = (BitmapDrawable) getResources().getDrawable(R.drawable.car);
                    Bitmap b = bitmapDrawable.getBitmap();
                    Bitmap smallCar = Bitmap.createScaledBitmap(b, 60, 72, false);
                    markerOptions1.icon(BitmapDescriptorFactory.fromBitmap(smallCar));
                    markerOptions1.rotation(Float.parseFloat(get_cab_position_response_data.getString("cab_bearing")));

                    nearby_cab = gMap.addMarker(markerOptions1);

                    handler.postDelayed(this, 10000);
                } else if (response_data.getString("status").equals("2")) {
                    ll_cancel.setClickable(false);
                    handler.removeCallbacksAndMessages(runnable);
                    if (nearby_cab != null) {
                        nearby_cab.remove();
                    }

                    tripStarted = true;
                    cab.setVisibility(View.VISIBLE);
                } else if (response_data.getString("status").equals("3")) {
                    ll_cancel.setClickable(false);
                    handler.removeCallbacksAndMessages(runnable);
                } else {
                    handler.postDelayed(this, 10000);
                }

            } catch (Exception e) {
                handler.postDelayed(this, 10000);
            }


        }
    };




    @Override
    public void onMapReady(GoogleMap googleMap) {
        gMap=googleMap;
        gMap.addMarker(place1).setTitle("Source");
        gMap.addMarker(place2).setTitle("dESTINATION");

        CameraPosition cameraPosition=CameraPosition.builder()
                .target(new LatLng(sourceLatitude,sourceLongitude))
                .zoom(10)
                .bearing(0)
                .tilt(45)
                .build();
        gMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition),5000,null);
    }





    public JSONObject call_api(String api_url, String request_data) {
        try {
            URL url = new URL(api_url);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("POST");
            conn.setDoInput(true);
            conn.setDoOutput(true);
            OutputStream os = conn.getOutputStream();
            BufferedWriter writer = new BufferedWriter(
                    new OutputStreamWriter(os, "UTF-8"));

            writer.write(request_data);
            writer.flush();
            writer.close();
            os.close();

            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            String line = "";
            String response = "";
            while ((line = bufferedReader.readLine()) != null) {
                response += line;
            }

            Log.d("API response", response);

            JSONObject response_data = new JSONObject(response);
            return response_data;

        } catch (Exception e) {
//            Toast.makeText(getApplicationContext(),e.toString(),Toast.LENGTH_LONG).show();
        }

        return null;
    }


    public double calculationByDistance(LatLng StartP, LatLng EndP) {
        int Radius=6371;//radius of earth in Km
        double lat1 = StartP.latitude;
        double lat2 = EndP.latitude;
        double lon1 = StartP.longitude;
        double lon2 = EndP.longitude;
        double dLat = Math.toRadians(lat2-lat1);
        double dLon = Math.toRadians(lon2-lon1);
        double a = Math.sin(dLat/2) * Math.sin(dLat/2) +
                Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2)) *
                        Math.sin(dLon/2) * Math.sin(dLon/2);
        double c = 2 * Math.asin(Math.sqrt(a));
        double result=Radius * c;
        Log.i("result",""+result);
        return result;
    }


    public double totalDistance(ArrayList<LatLng> points)
    {
        double totalDist = 0;

        for(int i = 0 ; i < points.size() - 1 ; i++)
        {
            totalDist += calculationByDistance(points.get(i) , points.get(i+1));
        }
        return totalDist;
    }


}
