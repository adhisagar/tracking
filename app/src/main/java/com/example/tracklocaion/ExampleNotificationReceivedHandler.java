package com.example.tracklocaion;

import android.util.Log;

import com.onesignal.OSNotification;
import com.onesignal.OneSignal;

import org.json.JSONObject;

/**
 * Created by Anup on 7/22/2017.
 */

class ExampleNotificationReceivedHandler implements OneSignal.NotificationReceivedHandler {
    public String customerName = "bookings", pickupLocation = "Booking", customerPhone = "9874561230", fare = "25", otp = "4568", ride_id = "012", cab_id = "258";
    Double startLat = 28.571100, startLng = 77.328529, endLat = 28.640520, endLng = 77.282707;

    @Override
    public void notificationReceived(OSNotification notification) {
        JSONObject data = notification.payload.additionalData;

        if (data != null) {
            customerName = data.optString("customerName", null);
            customerPhone = data.optString("customerPhone", null);
            startLat = Double.parseDouble(data.optString("src_lat", null));
            startLng = Double.parseDouble(data.optString("src_lng", null));
            endLat = Double.parseDouble(data.optString("dest_lat", null));
            endLng = Double.parseDouble(data.optString("dest_lng", null));
            fare = data.optString("fare", null);
            otp = data.optString("otp", null);
            ride_id = data.optString("ride_id", null);
            cab_id = data.optString("cab_id", null);
            if (customerName != null)
                Log.i("OneSignalExample", "customkey set with value: ");
        }
    }
}